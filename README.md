# README #

Sample Code for Schibsted Media Group. Used Model-View-Presenter pattern.

### Libraries used ###

* RxJava/RxAndroid: Observable pattern to emit and consume events with Observable and Subscriber objects.
* Gson: Json parsing
* Retrofit: API client
* Glide: Remote image loading and caching
* Android support libraries