package com.albertomanzano.schibsted.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Model class to represent a Country
 */
public class Country implements Parcelable {

    @SerializedName("name") private String name;
    @SerializedName("capital") private String capital;
    @SerializedName("altSpellings") private List<String> altSpellings;
    @SerializedName("region") private String region;
    @SerializedName("population") private long population;
    @SerializedName("alpha2Code") private String alpha2Code;
    @SerializedName("currencies") private List<String> currencies;
    @SerializedName("languages") private List<String> languages;

    /**
     * Returns the name of the country
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the capital of the country
     * @return String
     */
    public String getCapital() {
        return capital;
    }

    /**
     * Returns the region of the country
     * @return String
     */
    public String getRegion() {
        return region;
    }

    /**
     * Returns the population of the country
     * @return long
     */
    public long getPopulation() {
        return population;
    }

    /**
     * Returns the 2-digit code of the country
     * @return String
     */
    public String getAlpha2Code() {
        return alpha2Code;
    }

    /**
     * Returns the list of currencies of the country, one per line
     * @return String
     */
    public String getCurrencies() {
        return formattedString(currencies);
    }

    /**
     * Returns the list of languages of the country, one per line
     * @return String
     */
    public String getLanguages() {
        return formattedString(languages);
    }

    /**
     * Returns the list of spellings of the country, one per line
     * @return String
     */
    public String getAltSpellings() {
        return formattedString(altSpellings);
    }

    // Returns a String representation of a List<String>
    private String formattedString(List<String> list) {
        String formattedString = "";
        for(int i=0; i<list.size(); i++) {
            String string = list.get(i);
            formattedString = formattedString + string;

            // Add new line character for all lines but the last one
            if (i != list.size() - 1) {
                formattedString = formattedString + "\n";
            }
        }
        return formattedString;
    }

    // Parcelable methods
    protected Country(Parcel in) {
        name = in.readString();
        capital = in.readString();
        region = in.readString();
        population = in.readLong();
        alpha2Code = in.readString();
        if (in.readByte() == 0x01) {
            currencies = new ArrayList<>();
            in.readList(currencies, String.class.getClassLoader());
        } else {
            currencies = null;
        }
        if (in.readByte() == 0x01) {
            altSpellings = new ArrayList<>();
            in.readList(altSpellings, String.class.getClassLoader());
        } else {
            altSpellings = null;
        }
        if (in.readByte() == 0x01) {
            languages = new ArrayList<>();
            in.readList(languages, String.class.getClassLoader());
        } else {
            languages = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(capital);
        dest.writeString(region);
        dest.writeLong(population);
        dest.writeString(alpha2Code);
        if (currencies == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(currencies);
        }
        if (altSpellings == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(altSpellings);
        }
        if (languages == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(languages);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Country> CREATOR = new Parcelable.Creator<Country>() {
        @Override
        public Country createFromParcel(Parcel in) {
            return new Country(in);
        }

        @Override
        public Country[] newArray(int size) {
            return new Country[size];
        }
    };
}