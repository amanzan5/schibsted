package com.albertomanzano.schibsted.presenter;

import com.albertomanzano.schibsted.model.Country;
import com.albertomanzano.schibsted.retrofit.RetrofitClient;
import com.albertomanzano.schibsted.view.CountryDetailView;

import java.util.List;

import rx.SingleSubscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Presenter that makes the API call to obtain a list of suggested countries for the name introduced
 */
public class CountryDetailPresenter implements Presenter<CountryDetailView> {

    private CountryDetailView countryDetailView;
    private Subscription subscription;

    @Override
    public void attachView(CountryDetailView view) {
        // Keep a copy of the view where we will present results
        countryDetailView = view;
    }

    @Override
    public void detachView() {
        // Release view and unsubscribe if needed
        countryDetailView = null;
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    /**
     * Makes API call to get list of countries using Retrofit + RxJava
     */
    public void getCountry(String country) {
        subscription = RetrofitClient.getInstance().getCountryApi().getCountry(country)
                .observeOn(AndroidSchedulers.mainThread()) // observe on UI thread
                .subscribeOn(Schedulers.io()) // make the call on a background thread
                .subscribe(new SingleSubscriber<List<Country>>() {
                    @Override
                    public void onSuccess(List<Country> list) {
                        // Since the api returns a list of countries, just get the first one
                        countryDetailView.loadCountry(list.get(0));
                    }

                    @Override
                    public void onError(Throwable error) {
                        countryDetailView.showError(error.getMessage());
                    }
                });
    }
}
