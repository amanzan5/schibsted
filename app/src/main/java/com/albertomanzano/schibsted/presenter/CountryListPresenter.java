package com.albertomanzano.schibsted.presenter;

import com.albertomanzano.schibsted.model.Country;
import com.albertomanzano.schibsted.retrofit.RetrofitClient;
import com.albertomanzano.schibsted.view.CountryListView;

import java.util.List;

import rx.SingleSubscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Presenter that makes the API call to obtain a list of countries
 */
public class CountryListPresenter implements Presenter<CountryListView> {

    private CountryListView countryListView;
    private Subscription subscription;

    @Override
    public void attachView(CountryListView view) {
        // Keep a copy of the view where we will present results
        countryListView = view;
    }

    @Override
    public void detachView() {
        // Release view and unsubscribe if needed
        countryListView = null;
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    /**
     * Makes API call to get list of countries using Retrofit + RxJava
     */
    public void getAllCountries() {
        subscription = RetrofitClient.getInstance().getCountryApi().getAllCountries()
                .observeOn(AndroidSchedulers.mainThread()) // observe on UI thread
                .subscribeOn(Schedulers.io()) // make the call on a background thread
                .subscribe(new SingleSubscriber<List<Country>>() {
                    @Override
                    public void onSuccess(List<Country> value) {
                        countryListView.showCountries(value);
                    }

                    @Override
                    public void onError(Throwable error) {
                        countryListView.showError(error.getMessage());
                    }
                });
    }
}
