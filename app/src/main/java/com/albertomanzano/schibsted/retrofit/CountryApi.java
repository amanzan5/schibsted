package com.albertomanzano.schibsted.retrofit;

import com.albertomanzano.schibsted.model.Country;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Single;

/**
 * Brastlewark interface for Retrofit
 */
public interface CountryApi {

    @GET("all")
    Single<List<Country>> getAllCountries();

    @GET("name/{country}")
    Single<List<Country>> getCountry(@Path("country") String country);
}
