package com.albertomanzano.schibsted.retrofit;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Singleton object containing all Retrofit objects needed within the app
 */
public class RetrofitClient {

    // URL for the API call to get countries
    public static final String COUNTRIES_BASE_URL = "https://restcountries.eu/rest/v1/";

    private static RetrofitClient sRetrofitClient;
    private CountryApi countryApi;

    /**
     * Restrict access to constructor to assure Singleton pattern usage
     */
    private RetrofitClient() {}

    /**
     * Singleton call to get a static instance
     * @return RetrofitClient
     */
    public static synchronized RetrofitClient getInstance() {
        if (sRetrofitClient == null) {
            sRetrofitClient = new RetrofitClient();
        }

        return sRetrofitClient;
    }

    /**
     * Creates countryApi if it's not already created
     * @return CountryApi
     */
    public CountryApi getCountryApi() {
        if (countryApi == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(COUNTRIES_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            countryApi = retrofit.create(CountryApi.class);
        }

        return countryApi;
    }
}
