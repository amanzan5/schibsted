package com.albertomanzano.schibsted.view;

import android.content.Context;

/**
 * Interface that represents MVP's View. Base for all the future Views within the app
 */
public interface View {

    // Returns a copy of the context
    Context getContext();
}
