package com.albertomanzano.schibsted.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.albertomanzano.schibsted.R;
import com.albertomanzano.schibsted.model.Country;
import com.albertomanzano.schibsted.presenter.CountryDetailPresenter;

/**
 * Home Activity that offers the possibility of loading a specific country detail activity
 * or an activity with a list of all available countries
 */
public class MainActivity extends AppCompatActivity implements CountryDetailView {

    private EditText enteredName;
    private Button showCountry;
    private CountryDetailPresenter countryDetailPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        enteredName = (EditText) findViewById(R.id.entered_name);
        handleEnterKeyPress();

        Button allCountries = (Button) findViewById(R.id.all_countries);
        if (allCountries != null) {
            allCountries.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, CountryListActivity.class));
                }
            });
        }

        countryDetailPresenter = new CountryDetailPresenter();
        countryDetailPresenter.attachView(this);

        showCountry = (Button) findViewById(R.id.show_country);
        showCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countryDetailPresenter.getCountry(enteredName.getText().toString());
            }
        });
    }

    // If user press enter key while the EditText has focus, it will seach automatically
    private void handleEnterKeyPress() {
        enteredName.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    showCountry.callOnClick();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (enteredName.hasFocus()) {
            enteredName.clearFocus();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        countryDetailPresenter.detachView();

        super.onDestroy();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void loadCountry(Country country) {
        // Put the text in the Intent and start CountryDetailActivity
        Intent intent = new Intent(MainActivity.this, CountryDetailActivity.class);
        intent.putExtra(Country.class.getName(), country);
        startActivity(intent);
    }

    @Override
    public void showError(String errorMessage) {
        Toast.makeText(this, "No countries found for that name", Toast.LENGTH_SHORT).show();
    }
}
