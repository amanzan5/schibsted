package com.albertomanzano.schibsted.view;

import com.albertomanzano.schibsted.model.Country;

import java.util.List;

/**
 * Interface that represents MVP's View for the CountryListActivity
 */
public interface CountryListView extends View {

    // Displays list of countries received from the presenter
    void showCountries(List<Country> countryList);

    // Displays error received from the presenter
    void showError(String errorMessage);
}
