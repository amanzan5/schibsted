package com.albertomanzano.schibsted.view;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.albertomanzano.schibsted.R;
import com.albertomanzano.schibsted.model.Country;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.Locale;

/**
 * Detail Activity to display information about the selected country
 */
public class CountryDetailActivity extends AppCompatActivity {

    private CollapsingToolbarLayout collapsingToolbarLayout;
    private ImageView countryFlag;
    private ProgressBar loadingFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Slide in from right animation for this activity, slide out to left for the previous one
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);

        setContentView(R.layout.activity_country_detail);

        init();
    }

    private void init() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        countryFlag = (ImageView) findViewById(R.id.country_flag);
        loadingFlag = (ProgressBar) findViewById(R.id.loading_flag);

        loadCountryInfo();
    }

    /**
     * Clicks on the back button will show a slide in/out animation
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
    }

    /**
     * Clicks on the Toolbar's back button will show a slide in/out animation
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    // Displays the info about the country
    private void loadCountryInfo() {
        // Get the country that was sent by the CountryListActivity, if any
        final Country country = getIntent().getExtras().getParcelable(Country.class.getName());

        if (country != null) {
            loadCountryInfo(country);
        }
    }

    private void loadCountryInfo(Country country) {
        // Show name of the country in the collapsing toolbar
        collapsingToolbarLayout.setTitle(country.getName());

        // Load flag image using Glide
        Glide.with(this)
                .load(buildUrl(country.getAlpha2Code()))
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        Toast.makeText(CountryDetailActivity.this, "Glide failed: " + e.getMessage(),
                                Toast.LENGTH_SHORT).show();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        loadingFlag.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(countryFlag);

        // Show more info about the country
        setText((TextView) findViewById(R.id.name), country.getName());
        setText((TextView) findViewById(R.id.capital), country.getCapital());
        setText((TextView) findViewById(R.id.alt_spellings), country.getAltSpellings());
        setText((TextView) findViewById(R.id.region), country.getRegion());
        setText((TextView) findViewById(R.id.population), Long.toString(country.getPopulation()));
        setText((TextView) findViewById(R.id.code), country.getAlpha2Code());
        setText((TextView) findViewById(R.id.currencies), country.getCurrencies());
        setText((TextView) findViewById(R.id.languages), country.getLanguages());
    }

    // Sets the text to the received textview
    private void setText(TextView text, String info) {
        text.setText(info);
    }

    // Returns the URL to obtain the flag of the country
    private String buildUrl(String countryCode) {
        return CountryAdapter.FLAGS_URL + countryCode.toLowerCase(Locale.getDefault())
                + CountryAdapter.FLAGS_EXTENSION;
    }
}