package com.albertomanzano.schibsted.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.albertomanzano.schibsted.R;
import com.albertomanzano.schibsted.model.Country;
import com.albertomanzano.schibsted.presenter.CountryListPresenter;

import java.util.List;

/**
 * Activity with an list of the available countries and a filter option within the top toolbar
 */
public class CountryListActivity extends AppCompatActivity implements CountryListView {

    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private CountryAdapter countryAdapter;
    private TextView errorText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Slide in from right animation for this activity, slide out to left for the previous one
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);

        setContentView(R.layout.activity_country_list);

        init();
    }

    private void init() {
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        errorText = (TextView) findViewById(R.id.error_text);
        recyclerView = (RecyclerView) findViewById(R.id.countries_recyclerview);

        initRecyclerView();

        CountryListPresenter countryListPresenter = new CountryListPresenter();
        countryListPresenter.attachView(this);
        countryListPresenter.getAllCountries();
    }

    private void initRecyclerView() {
        countryAdapter = new CountryAdapter(this);
        recyclerView.setAdapter(countryAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                countryAdapter.filter(newText);
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Clicks on the back button will show a slide in/out animation
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
    }

    /**
     * Clicks on the Toolbar's back button will show a slide in/out animation
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        // Detects clicks on the back arrow icon within the top toolbar
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(menuItem);
        }
    }

    @Override
    public void showCountries(List<Country> countryList) {
        // Hide progress layout and show textViews
        progressBar.setVisibility(View.GONE);
        countryAdapter.setData(countryList);
        countryAdapter.notifyDataSetChanged();
    }

    @Override
    public void showError(String errorMessage) {
        progressBar.setVisibility(View.GONE);
        errorText.setVisibility(View.VISIBLE);
        errorText.setText(getString(R.string.error, errorMessage));
    }

    @Override
    public Context getContext() {
        return this;
    }
}
