package com.albertomanzano.schibsted.view;

import com.albertomanzano.schibsted.model.Country;

/**
 * Interface that represents MVP's View for the a country detail information
 */
public interface CountryDetailView extends View {

    // Displays country received from the presenter
    void loadCountry(Country country);

    // Displays error received from the presenter
    void showError(String errorMessage);
}
