package com.albertomanzano.schibsted.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.albertomanzano.schibsted.R;
import com.albertomanzano.schibsted.model.Country;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * Adapter for the RecyclerView that displays the list of countries.
 *
 * Contains a filter method to display only the countries that matches a search query
 */
public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryViewHolder> {

    // URL to get flag images
    public static final String FLAGS_URL = "http://www.geonames.org/flags/x/";

    // Extension for the flag images
    public static final String FLAGS_EXTENSION = ".gif";

    private final Context context;
    private List<Country> countryList;
    private List<Country> initialCountryList = new ArrayList<>();

    // Constructor
    public CountryAdapter(Context context) {
        this.context = context;
        this.countryList = new ArrayList<>();
    }

    @Override
    public CountryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(context).inflate(R.layout.country_row, parent, false);
        return new CountryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CountryViewHolder holder, int position) {
        final Country country = countryList.get(position);
        holder.bind(country);
    }

    @Override
    public int getItemCount() {
        return countryList.size();
    }

    // Sets the list of countries
    public void setData(List<Country> countryList) {
        this.countryList = new ArrayList<>(countryList);

        // Keep a copy of the initial list
        initialCountryList.addAll(countryList);
    }

    // Filters the list of countryList according to the received text
    public void filter(String text) {
        if (text.isEmpty()) {
            // If text is empty, all the initial list of countries should be displayed
            countryList.clear();
            countryList.addAll(initialCountryList);
        } else {
            List<Country> result = new ArrayList<>();
            text = text.toLowerCase();

            // Check which country of the initial list contains the received text
            for (Country item: initialCountryList){
                if(item.getName().toLowerCase().contains(text)){
                    result.add(item);
                }
            }
            countryList.clear();

            // Set the filtered results
            countryList.addAll(result);
        }

        // Notify the adapter that the dataset has changed
        notifyDataSetChanged();
    }

    /**
     * ViewHolder for a Country object
     */
    class CountryViewHolder extends RecyclerView.ViewHolder {

        private final TextView name;
        private final ImageView thumbnail;

        // Constructor
        public CountryViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            thumbnail = (ImageView) itemView.findViewById(R.id.thumbnail);
        }

        // Binds Country object
        public void bind(final Country country) {
            name.setText(country.getName());

            // Use Glide to load remote image
            Glide.with(context).load(buildUrl(country.getAlpha2Code())).into(thumbnail);

            // If item is clicked, launch new activity to display more info about the country
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Put the Country object in the Intent and start CountryDetailActivity
                    Intent intent = new Intent(context, CountryDetailActivity.class);
                    intent.putExtra(Country.class.getName(), country);
                    context.startActivity(intent);
                }
            });
        }

        // Returns the URL to obtain the flag of the country
        private String buildUrl(String countryCode) {
            return FLAGS_URL + countryCode.toLowerCase(Locale.getDefault()) + FLAGS_EXTENSION;
        }
    }
}
